import { Component } from '@angular/core';

@Component({
  selector: 'app-nothing-selected',
  template: `<p>Nothing selected</p>`,
  styles: [`
    p {
      color: #a7a7a7;
    }
  `]
})
export class NothingSelectedComponent {}
