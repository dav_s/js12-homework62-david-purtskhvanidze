export class Game {
  constructor(
    public gameName: string,
    public gameCover: string,
    public gamePlatform: string,
    public gameDescription: string,
  ) {}
}
