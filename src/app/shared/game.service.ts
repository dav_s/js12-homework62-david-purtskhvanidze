import { Game } from './game.model';
import { EventEmitter } from '@angular/core';

export class GameService {
  gamesChange = new EventEmitter<Game[]>();

  private games: Game[] = [
    new Game(
      'Metro Exodus',
      'https://upload.wikimedia.org/wikipedia/en/a/af/Cover_Art_of_Metro_Exodus.png',
      'pc',
      'Metro Exodus is a first-person shooter video game developed by 4A Games and published by Deep Silver. It is the third installment in the Metro video game trilogy based on Dmitry Glukhovsky\'s novels, following the events of Metro 2033 and Metro: Last Light. It released in 2019 for Microsoft Windows, PlayStation 4, Xbox One and Stadia and in 2020 for Amazon Luna. A Linux and macOS conversion was released in 2021. An enhanced version of the game released for Microsoft Windows on May 6, 2021 and was later released for PlayStation 5 and Xbox Series X/S on June 18, 2021. The game received positive reviews from critics.'
    ),

    new Game(
      'The Last Of Us',
      'https://upload.wikimedia.org/wikipedia/en/4/46/Video_Game_Cover_-_The_Last_of_Us.jpg',
      'playstation',
      'The Last of Us is a 2013 action-adventure game developed by Naughty Dog and published by Sony Computer Entertainment. Players control Joel, a smuggler tasked with escorting a teenage girl, Ellie, across a post-apocalyptic United States. The Last of Us is played from a third-person perspective. Players use firearms and improvised weapons, and can use stealth to defend against hostile humans and cannibalistic creatures infected by a mutated fungus in the genus Cordyceps. In the online multiplayer mode, up to eight players engage in cooperative and competitive gameplay.'
    ),

    new Game(
      'The Last Of Us Part II',
      'https://upload.wikimedia.org/wikipedia/en/4/4f/TLOU_P2_Box_Art_2.png',
      'playstation',
      'The Last of Us Part II is a 2020 action-adventure game developed by Naughty Dog and published by Sony Interactive Entertainment for the PlayStation 4. Set five years after The Last of Us (2013), the game focuses on two playable characters in a post-apocalyptic United States whose lives intertwine: Ellie, who sets out for revenge after suffering a tragedy, and Abby, a soldier who becomes involved in a conflict between her militia and a religious cult. The game is played from the third-person perspective and allows the player to fight human enemies and cannibalistic zombie-like creatures with firearms, improvised weapons, and stealth.'
    ),

    new Game(
      'Forza Horizon 4',
      'https://upload.wikimedia.org/wikipedia/en/8/87/Forza_Horizon_4_cover.jpg',
      'xbox',
      'Forza Horizon 4 is a 2018 racing video game developed by Playground Games and published by Microsoft Studios.[3] It was released on 2 October 2018 on Xbox One and Microsoft Windows after being announced at Xbox\'s E3 2018 conference.[4][5] An enhanced version of the game was released on Xbox Series X/S on 10 November 2020. The game is set in a fictionalised representation of areas of Great Britain.[3][4][5] It is the fourth Forza Horizon title and eleventh instalment in the Forza series. The game is noted for its introduction of changing seasons to the series, as well as featuring several content-expanding updates which have included new game modes.'
    ),

    new Game(
      'Halo Infinite',
      'https://upload.wikimedia.org/wikipedia/en/1/14/Halo_Infinite.png',
      'xbox',
      'Halo Infinite is a 2021 first-person shooter game developed by 343 Industries and published by Xbox Game Studios. It is the seventh main entry in the Halo series, following 2015\'s Halo 5: Guardians.\n' +
      '\n' +
      'The campaign follows the human supersoldier Master Chief and his fight against the enemy Banished on the Forerunner ringworld Zeta Halo. Unlike previous installments in the series, the multiplayer portion of the game is free-to-play.\n' +
      '\n' +
      'Infinite was planned to be released as a launch title with the Xbox Series X/S on November 10, 2020, but was delayed in August 2020. It was eventually released on December 8, 2021, for Microsoft Windows, Xbox One, and Xbox Series X/S. Halo Infinite\'s multiplayer component has been in open beta since November 15, 2021, which was to commemorate the franchise and Xbox\'s 20th anniversary. The game received generally favorable reviews.'
    ),

  ];

  getGameByName(gameName: string) {
    const index = this.games.findIndex(g => g.gameName === gameName);
    return this.games[index];
  }

  getGamesByPlatform(platform: string) {
    return this.games.filter(g => g.gamePlatform === platform)
  }

  addGame(game: Game) {
    this.games.push(game);
    this.gamesChange.emit(this.games);
  }

}
