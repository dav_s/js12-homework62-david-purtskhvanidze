import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AddGameComponent } from './add-game/add-game.component';
import { GamesComponent } from './games/games.component';
import { GameComponent } from './games/game/game.component';
import { NotFoundComponent } from './not-found.component';
import { NothingSelectedComponent } from './nothing-selected.component';
import { RouterModule, Routes } from '@angular/router';
import { GameService } from './shared/game.service';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {path: '', component: HomeComponent, children: [
      {path: '', component: NothingSelectedComponent},
      {path: 'by-platform/:platform-name', component: GamesComponent},
  ]},
  {path: 'game/:game-name', component: GameComponent},
  {path: 'add', component: AddGameComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  declarations: [
    HomeComponent,
    AppComponent,
    AddGameComponent,
    GamesComponent,
    GameComponent,
    NotFoundComponent,
    NothingSelectedComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  providers: [GameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
