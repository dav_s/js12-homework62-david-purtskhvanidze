import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Game } from '../shared/game.model';
import { GameService } from '../shared/game.service';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {
  public gamesByPlatform!: Game[];

  constructor(
    private route: ActivatedRoute,
    private gameService: GameService
    ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const platformName = params['platform-name'];
      this.gamesByPlatform = this.gameService.getGamesByPlatform(platformName);
    });
  }

}
