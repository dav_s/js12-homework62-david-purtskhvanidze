import { Component, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { GameService } from '../shared/game.service';
import { Game } from '../shared/game.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-game',
  templateUrl: './add-game.component.html',
  styleUrls: ['./add-game.component.css']
})
export class AddGameComponent {
  @ViewChild('gameAddForm') gameAddForm!: ElementRef;
  @ViewChild('gameNameInput') gameNameInput!: ElementRef;
  @ViewChild('gameCoverUrlInput') gameCoverUrlInput!: ElementRef;
  @ViewChild('gamePlatformSelect') gamePlatformSelect!: ElementRef;
  @ViewChild('gameDescriptionInput') gameDescriptionInput!: ElementRef;

  constructor(
    private gameService: GameService,
    private router: Router,
    private renderer: Renderer2
  ) {}

  addGame() {
    const form = this.gameAddForm.nativeElement;
    const gameName: string = this.gameNameInput.nativeElement.value;
    const gameCover: string = this.gameCoverUrlInput.nativeElement.value;
    const gamePlatform: string = this.gamePlatformSelect.nativeElement.value;
    const gameDescription: string = this.gameDescriptionInput.nativeElement.value;

    const game = new Game(gameName, gameCover, gamePlatform, gameDescription);

    if (gameName.length && gameCover.length && gamePlatform.length && gameDescription.length) {
      this.gameService.addGame(game);
      void this.router.navigate(['/']);
    } else {
      this.renderer.addClass(form, 'no-valid');
    }

  }
}
